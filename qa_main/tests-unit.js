/* Created by instancetype on 8/8/14. */
 /* jshint bitwise : true, eqeqeq : true, forin : true, noarg : true, noempty : true, nonew : true,
   asi : true, esnext : true, laxcomma : true, sub : true, browser : true, node : true, phantom : true */

// mocha --harmony -u tdd -R spec qa/tests-unit.js
const
  color = require('../lib/color')
, expect = require('chai').expect

suite('Color tests', function() {

  test('getColor() should return a color', function() {
    expect(typeof color.getColor() === 'string')
  })

})