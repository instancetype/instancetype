/**
 * Created by instancetype on 8/13/14.
 */
 /* jshint bitwise : true, eqeqeq : true, forin : true, noarg : true, noempty : true, nonew : true,
   asi : true, esnext : true, laxcomma : true, sub : true, browser : true, node : true, phantom : true */
const
  loadtest = require('loadtest')
, expect = require('chai').expect

suite('Stress Tests', function() {

  test('Homepage should handle 100 requests in a second', function(done) {
    var options =
      { url : 'http://localhost:3000'
      , concurrency : 4
      , maxRequests : 100
      }
    loadtest.loadTest(options, function(err, results) {
      expect(!err)
      expect(results.totalTimeSeconds < 1)
      done()
    })
  })
})