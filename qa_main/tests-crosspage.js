/* Created by instancetype on 8/8/14. */
 /* jshint bitwise : true, eqeqeq : true, forin : true, noarg : true, noempty : true, nonew : true,
   asi : true, esnext : true, laxcomma : true, sub : true, browser : true, node : true, phantom : true */

// $ mocha -u tdd -R spec qa/tests-crosspage.js
const
  Browser = require('zombie')
, assert = require('chai').assert

var browser

suite('Cross-Page Tests', function() {

  setup(function() {
    browser = new Browser() // { debug : true }
  })

  test(
    'requesting group rate quote from the mon river tour ' +
    'page should populate the referrer field'

  , function(done) {
      var referrer = 'http://localhost:3000/tours/mon-river'

      browser.visit(referrer, function() {
        browser.clickLink('.requestGroupRate', function() {
          assert(browser.field('referrer').value === referrer)
          done()
        })
      })
    }
  )

  test(
    'requesting group rate quote from the ohio river tour ' +
    'page should populate the referrer field'

  , function(done) {
      var referrer = 'http://localhost:3000/tours/ohio-river'

      browser.visit(referrer, function() {
        browser.clickLink('.requestGroupRate', function() {
          assert(browser.field('referrer').value === referrer)
          done()
        })
      })
    }
  )

  test(
    'visiting the "request group rate" page directly ' +
    'should leave an empty referrer field'

  , function(done) {
      browser.visit('http://localhost:3000/tours/request-grouprate', function() {
        assert(browser.field('referrer').value === '')
        done()
      })
    }
  )
})