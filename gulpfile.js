/**
 * Created by instancetype on 8/17/14.
 */
 /* jshint bitwise : true, eqeqeq : true, forin : true, noarg : true, noempty : true, nonew : true,
   asi : true, esnext : true, laxcomma : true, sub : true, browser : true, node : true, phantom : true */
const
  jshint = require('gulp-jshint')
, stylish = require('jshint-stylish')
, mocha = require('gulp-mocha')
, less = require('gulp-less')
, gulp = require('gulp')


gulp.task('lint', ['test_api'], function(done) {
  var scripts =  [ 'instancetype.js'
                 , 'public/js/**/*.js'
                 , 'lib/**/*.js'
                 , 'public/qa_*/**/*.js'
                 , 'qa/**/*.js'
                 , 'gulpfile.js'
                 ]

  return gulp.src(scripts)
    .pipe(jshint())
    .pipe(jshint.reporter(stylish, { verbose : true }))
  done()
})

gulp.task('test_api', function(done) {
  return gulp.src(['qa_api/tests-*.js'], {read : false})
    .pipe(mocha(
      { reporter : 'spec'
      , ui : 'tdd'
      }
    ))
  done(err)
})

// Serializes testing to avoid test_api conflicting with zombie.js
gulp.task('test_main', ['test_api', 'lint'], function() {
  return gulp.src(['qa_main/tests-*.js'], {read : false})
    .pipe(mocha(
      { reporter : 'spec'
      , ui : 'tdd'
      }
    ))
})

gulp.task('less', function() {
  return gulp.src('less/main.less')
    .pipe(less(
      { customFunctions : {
          'static' : function(less, name) {
            console.log('static was called')
            return 'url("' + require('./lib/static').map(name.value) + '")'
          }

      }}

    ))
    .pipe(gulp.dest('public/styles'))
})

gulp.task('default', ['test_main'])

gulp.task('watch', function() {
  gulp.watch('less/main.less', ['less'])
})

