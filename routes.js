/**
 * Created by instancetype on 8/14/14.
 */
 /* jshint bitwise : true, eqeqeq : true, forin : true, noarg : true, noempty : true, nonew : true, expr: true,
   asi : true, esnext : true, laxcomma : true, sub : true, browser : true, node : true, phantom : true */
const
  main = require('./handlers/main')
, vacations = require('./handlers/vacations')
, tours = require('./handlers/tours')
, contest = require('./handlers/contest')
, test = require('./handlers/test')

module.exports = function(app) {

  app.get('/', main.home)
  app.get('/about', main.about)

  app.get('/jquery-test', test.jQuery)

  app.get('/nursery-rhyme', main.nurseryRhyme)
  app.get('/data/nursery-rhyme', main.nurseryRhymeData)

  app.get('/tours/mon-river', tours.monRiver)
  app.get('/tours/ohio-river', tours.ohioRiver)
  app.get('/tours/request-grouprate', tours.requestGrouprate)

  app.get('/vacations', vacations.list)
  app.get('/notify-me-when-in-season', vacations.notifyShow)
  app.post('/notify-me-when-in-season', vacations.notify)
  app.get('/set-currency/:currency', vacations.setCurrency)

  app.get('/newsletter', main.newsletter)
  app.post('/process', main.process)

  app.get('/thank-you', main.thankYou)
  app.get('/error', main.error)

  app.get('/contest/cat-photo', contest.catPhoto)
  app.post('/contest/cat-photo/:year/:month', contest.catPhotoEntry)

  // emailService test route
  app.get('/mailtest', main.mailtest)
  // Error test routes
  app.get('/fail', test.fail)
  app.get('/epic-fail', test.epicFail)
}