/* Created by instancetype on 8/7/14. */
 /* jshint bitwise : true, eqeqeq : true, forin : true, noarg : true, noempty : true, nonew : true,
   asi : true, esnext : true, laxcomma : true, sub : true, browser : true, node : true, phantom : true */
const
  weather = require('./lib/weather')
, credentials = require('./credentials')
, Vacation = require('./models/vacation')
, Attraction = require('./models/attraction')

const
  http = require('http')
, express = require('express')
, favicon = require('serve-favicon')
, exphbs = require('express3-handlebars')
    .create({ defaultLayout : 'main'
            , extname       : '.hbs'
            , helpers : { section : function(name, options) {
                            if (!this._sections) this._sections = {}
                            this._sections[name] = options.fn(this)
                            return null
                          }
                        , static : function(name) {
                            return require('./lib/static').map(name)
                          }
                        }
            }
    )
, bodyParser = require('body-parser')
, mongoose = require('mongoose')
, opts = { server : { socketOptions : { keepAlive : 1 }}}
, MongoSessionStore = require('session-mongoose')(require('connect'))
, sessionStore = new MongoSessionStore({ url : credentials.mongo.development.connectionString })
, cookieParser = require('cookie-parser')
, expressSession = require('express-session')
, rest = require('connect-rest')
, vhost = require('vhost')

const VALID_EMAIL_REGEX =
  /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)+$/


// Initial DB seeding
Vacation.find(function(err, vacations) {
  if (vacations.length) return

  new Vacation(
    { name : 'Pittsburgh Day Trip'
    , slug : 'pittsburgh-day-trip'
    , category : 'Day Trip'
    , sku : 'PGH87'
    , description : 'Yinz can paddle up the Ohio!'
    , priceInCents : 4995
    , tags : [ 'day trip', 'ohio river', 'canoe', 'pittsburgh' ]
    , isInSeason : true
    , isAvailable : true
    , requiresWaiver : true
    , maximumGuests : 16
    , notes : ''
    , packagesSold : 0
    }
  ).save()

  new Vacation(
    { name : 'Shadyside Getaway'
    , slug : 'shadyside-getaway'
    , category : 'Weekend Getaway'
    , sku : 'SS99'
    , description : 'This trip makes no sense, but it sure does cost a lot!'
    , priceInCents : 99999
    , tags : [ 'weekend getaway', 'shadyside', 'expensive' ]
    , isInSeason : false
    , isAvailable : true
    , requiresWaiver : false
    , maximumGuests : 8
    , notes : ''
    , packagesSold : 0
    }
  ).save()

  new Vacation(
    { name : 'Blawnox Blowout'
    , slug : 'blawnox-blowout'
    , category : 'Weekend Getaway'
    , sku : 'B27'
    , description : 'Come visit me and my cats! It\'ll be great!'
    , priceInCents : 12999
    , tags : [ 'weekend getaway', 'blawnox', 'blowout', 'cats' ]
    , isInSeason : true
    , isAvailable : true
    , requiresWaiver : false
    , maximumGuests : 1
    , notes : 'This is a non-smoking environment.'
    , packagesSold : 0
    }
  ).save()
})

var
  app = express()
    .use(favicon(__dirname + '/public/img/favicon.ico'))

app
  .set('port', process.env.PORT || 3000)
  .engine('hbs', exphbs.engine)
  .set('view engine', 'hbs')

switch(app.get('env')) {
  case 'development':
    app.use(require('morgan')('dev'))
    mongoose.connect(credentials.mongo.development.connectionString, opts)
    break
  case 'production':
    app.use(require('express-logger')(
      { path: __dirname + '/log/requests.log' }
    ))
    mongoose.connect(credentials.mongo.production.connectionString, opts)
    break
  default :
    throw new Error('Unknown execution environment:', app.get('env'))
}

app
  .use(express.static(__dirname + '/public'))
  .use(bodyParser.urlencoded({ extended : true }))
  .use(cookieParser(credentials.cookieSecret))
  .use(expressSession(
    { store  : sessionStore
    , secret : credentials.cookieSecret
    , saveUninitialized : true
    , resave : true
    }
  ))

  .use(function(req, res, next) {
    var domain = require('domain').create()

    domain.on('error', function(err) {
      console.error('DOMAIN ERROR CAUGHT\n', err.stack)
      try {
        setTimeout(function(){
          console.error('Failsafe shutdown.')
          process.exit(1)
        }, 5000)
        var worker = require('cluster').worker
        if (worker) worker.disconnect()
        server.close()

        try {
          next(err)
        }
        catch(err) {
          console.error('Express error mechanism failed.\n', err.stack)
          res.statusCode = 500
          res.setHeader('content-type', 'text/plain')
          res.end('Server Error')
        }
      }
      catch(err) {
        console.error('Unable to send 500 response.\n', err.stack)
      }
    })
    domain.add(req)
    domain.add(res)
    domain.run(next)
  })

  .use(
    function(req, res, next) {
      res.locals.showTests =
        app.get('env') !== 'production' &&
          req.query.test === '1'
      next()
    }
  )

  .use(function(req, res, next) {
    if(!res.locals.partials) res.locals.partials = {}
    res.locals.partials.weather = weather.getWeatherData()
    next()
  })

  .use(function(req, res, next) {
    res.locals.flash = req.session.flash
    delete req.session.flash
    next()
  })

if (app.get('env') === 'development') {
  app.use(function(req, res, next) {
    var cluster = require('cluster')
    if (cluster.isWorker) console.log('Worker %d received request', cluster.worker.id)

    next()
  })
}

require('./routes')(app)

var autoViews = {}
  , fs = require('fs')
app.use(function(req, res, next) {
  var path = req.path.toLowerCase()

  if (autoViews[path]) return res.render(autoViews[path])
  if (fs.existsSync(__dirname + '/views' + path + '.hbs')) {
    autoViews[path] = path.replace(/^\//,'')
    return res.render(autoViews[path])
  }
  next()
})

app.use('/api', require('cors')())

rest.get('/attractions', function(req, content, cb){
  Attraction.find({ approved: true }, function(err, attractions){
    if (err) return cb({ error: 'Internal error.' })

    cb(null, attractions.map(function(a){
      return { name: a.name
             , description: a.description
             , location: a.location
             }
    }))
  })
})

rest.post('/attraction', function(req, content, cb){
  var a = new Attraction(
    { name: req.body.name
    , description: req.body.description
    , location: { lat: req.body.lat, lng: req.body.lng }
    , history: { event: 'created'
               , email: req.body.email
               , date: new Date()
               }
    , approved: false
  })
  a.save(function(err, a){
    if (err) return cb({ error: 'Unable to add attraction.' })
    cb(null, { id: a._id })
  })
})

rest.get('/attraction/:id', function(req, content, cb){
  Attraction.findById(req.params.id, function(err, a){
    if(err) return cb({ error: 'Unable to retrieve attraction.' })

    cb(null
    , { name: a.name
      , description: a.description
      , location: a.location
      }
    )
  })
})


var apiOptions = { context: '/'
                 , domain: require('domain').create()
                 }

apiOptions.domain.on('error', function(err){
  console.log('API domain error.\n', err.stack)

  setTimeout(function(){
    console.log('Server shutting down after API domain error.')
    process.exit(1)
  }, 5000)

  server.close()
  var worker = require('cluster').worker
  if(worker) worker.disconnect()
})

app.use(vhost('api.*', rest.rester(apiOptions)))


app.use(function(req, res) {
  res.status(404)
  res.render('404')
})

app.use(function(err, req, res, next) {
  console.error(err.stack)
  res.status(500)
  res.render('500')
})


function startServer() {
  server = http.createServer(app).listen(app.get('port'), function() {
    console.log(
        'Express started in ' +
        app.get('env') +
        ' mode on http://localhost:' +
        app.get('port') +
        '; press Ctrl-C to quit.'
    )
  })
}

if (require.main === module) {
  startServer()
}
else {
  module.exports = startServer
}
