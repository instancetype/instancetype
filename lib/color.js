/* Created by instancetype on 8/8/14. */
 /* jshint bitwise : true, eqeqeq : true, forin : true, noarg : true, noempty : true, nonew : true,
   asi : true, esnext : true, laxcomma : true, sub : true, browser : true, node : true, phantom : true */
const colors = [ 'red'
               , 'yellow'
               , 'orange'
               , 'green'
               , 'blue'
               , 'indigo'
               , 'violet'
               ]

exports.getColor = function() {
  'use strict';
  var i = Math.floor(Math.random() * colors.length)

  return colors[i]
}