/**
 * Created by aya on 8/11/14.
 */
function getWeatherData() {
  return {
    locations:
      [ { name : 'Pittsburgh'
        , forecastUrl : 'http://www.wunderground.com/US/PA/Pittsburgh.html'
        , iconUrl : 'http://icons-ak.wxug.com/i/c/k/cloudy.gif'
        , weather : 'Overcast'
        , temp : '54.1 F (12.3 C)'
        }

      , { name : 'Blawnox'
        , forecastUrl : 'http://www.wunderground.com/US/PA/Blawnox.html'
        , iconUrl : 'http://icons-ak.wxug.com/i/c/k/partlycloudy.gif'
        , weather : 'Partly Cloudy'
        , temp : '55.0 F (12.8 C)'
        }

      , { name : 'Shadyside'
        , forecastUrl : 'http://www.wunderground.com/US/PA/Shadyside.html'
        , iconUrl : 'http://icons-ak.wxug.com/i/c/k/rain.gif'
        , weather : 'Light Rain'
        , temp : '54.1 F (12.3 C)'
        }
      ]
  }
}

module.exports.getWeatherData = getWeatherData