/**
 * Created by instancetype on 8/13/14.
 */
 /* jshint bitwise : true, eqeqeq : true, forin : true, noarg : true, noempty : true, nonew : true,
   asi : true, esnext : true, laxcomma : true, sub : true, browser : true, node : true, phantom : true */
var nodemailer = require('nodemailer')

module.exports = function(credentials) {

  var mailTransport = nodemailer.createTransport(
    { service : 'Gmail'
    , auth : { user : credentials.gmail.user
             , pass : credentials.gmail.password
             }
    }
  )

  var from = '"Instancetype" <donotreply@example.com>'
  var errorRecipient = 'errorguy@example.com'

  return {
    send : function(to, subject, body, cb) {
      mailTransport.sendMail(
        { from : from
        , to   : to
        , subject : subject
        , html    : body
        , generateTextFromHtml : true
        }
      , function(err) {
          if (err) {
            console.error('Unable to send:', err)
            cb(err)
          }
          else {
            cb()
          }
        }
      )
    }

  , emailError : function(message, filename, exception) {
      var body = '<h1>Instancetype Site Error</h1>' +
                 'Message:<br><pre>' + message + '</pre><br>'

      if (exception) body += 'Exception:<br><pre>' + exception + '</pre><br>'
      if (filename) body += 'Filename:<br><pre>' + filename + '</pre><br>'

      mailTransport.sendMail(
        { from : from
        , to   : errorRecipient
        , subject : 'Instancetype Site Error'
        , html    : body
        , generateTextFromHtml : true
        }
      , function(err) {
          if (err) console.error('Unable to send:', err)
        }
      )
    }
  }
}