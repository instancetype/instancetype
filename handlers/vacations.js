/**
 * Created by instancetype on 8/15/14.
 */
 /* jshint bitwise : true, eqeqeq : true, forin : true, noarg : true, noempty : true, nonew : true,
   asi : true, esnext : true, laxcomma : true, sub : true, browser : true, node : true, phantom : true */
const
  Vacation = require('../models/vacation')
, VacationInSeasonListener = require('../models/vacationInSeasonListener')


function convertFromUSD(value, currency) {
  switch (currency) {
    case 'USD': return value * 1
    case 'GBP': return value * 0.6
    case 'BTC': return value * 0.0023707918444761
    default   : return NaN
  }
}

exports.list = function(req, res) {
  Vacation.find(
    { isAvailable : true }
  , function(err, vacations) {
      var currency = req.session.currency || 'USD'
      var context =
        { currency : currency
        , vacations : vacations.map(function(vacation) {
        return { sku : vacation.sku
               , name : vacation.name
               , description : vacation.description
               , price : convertFromUSD(vacation.priceInCents/100, currency)
               , isInSeason : vacation.isInSeason
               }
        })
      }
      switch (currency) {
        case 'USD':
          context.currencyUSD = 'selected'
          break
        case 'GBP':
          context.currencyGBP = 'selected'
          break
        case 'BTC':
          context.currencyBTC = 'selected'
          break
      }
      res.render('vacations', context)
    }
  )
}

exports.setCurrency = function(req, res) {
  req.session.currency = req.params.currency
  return res.redirect(303, '/vacations')
}

exports.notifyShow = function(req, res) {
  res.render('notify-me-when-in-season', { sku : req.query.sku })
}

exports.notify = function(req, res) {
  VacationInSeasonListener.update(
    { email  : req.body.email }
  , { $push  : { skus: req.body.sku }}
  , { upsert : true }
  , function(err) {
      if (err) {
        console.error(err.stack)
        req.session.flash =
          { type : 'danger'
          , intro : 'Uh oh...'
          , message : 'There was an error processing your request.'
          }
        return res.redirect(303, '/vacations')
      }
      req.session.flash =
        { type : 'success'
        , intro : 'Thank you!'
        , message : 'You\'ll be notified when this vacation is in season.'
        }
      return res.redirect(303, '/vacations')
    }
  )
}