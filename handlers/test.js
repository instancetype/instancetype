/**
 * Created by instancetype on 8/15/14.
 */
 /* jshint bitwise : true, eqeqeq : true, forin : true, noarg : true, noempty : true, nonew : true,
   asi : true, esnext : true, laxcomma : true, sub : true, browser : true, node : true, phantom : true */
exports.jQuery = function(req, res) {
  res.render('jquery-test', { layout : 'section-test'})
}

exports.fail = function(req, res) {
  throw new Error('Nope!')
}

exports.epicFail = function(req, res) {
  process.nextTick(function() {
    throw new Error('Boom!')
  })
}