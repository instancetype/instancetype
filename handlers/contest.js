/**
 * Created by instancetype on 8/15/14.
 */
 /* jshint bitwise : true, eqeqeq : true, forin : true, noarg : true, noempty : true, nonew : true, expr: true,
   asi : true, esnext : true, laxcomma : true, sub : true, browser : true, node : true, phantom : true */
const
  fs = require('fs')


var dataDir = __dirname + '/data'
  , catPhotoDir = dataDir + '/cat-photo'
fs.existsSync(dataDir) || fs.mkdirSync(dataDir)
fs.existsSync(catPhotoDir) || fs.mkdirSync(catPhotoDir)

function saveContestEntry(contestName, email, year, month, photoPath) {

}

exports.catPhoto = function(req, res) {
  var now = new Date()
  res.render(
    'contest/cat-photo'
  , { year : now.getFullYear()
    , month : now.getMonth()
    }
  )
}

exports.catPhotoEntry = function(req, res) {
  var form = new formidable.IncomingForm()
  form.parse(req, function(err, fields, files) {
    if (err) {
      req.session.flash =
        { type : 'danger'
        , intro : 'Uh oh.'
        , message : 'Your submisson could not be processed. Please try again.'
        }
      return res.redirect(303, '/contest/cat-photo')
    }
    var photo = files.photo
      , dir = catPhotoDir + '/' + Date.now()
      , path = dir + '/' + photo.name
    fs.mkdirSync(dir)
    fs.renameSync(photo.path, dir + '/' + photo.name)

    saveContestEntry('cat-photo', fields.email, req.params.year, req.params.month, path)
    req.session.flash =
      { type : 'success'
      , intro : 'Good luck!'
      , message : 'Your submission has been entered into the contest.'
      }
    return res.redirect(303, '/contest/cat-photo/entries')
  })
}