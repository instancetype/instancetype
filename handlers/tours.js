/**
 * Created by instancetype on 8/15/14.
 */
 /* jshint bitwise : true, eqeqeq : true, forin : true, noarg : true, noempty : true, nonew : true,
   asi : true, esnext : true, laxcomma : true, sub : true, browser : true, node : true, phantom : true */
exports.monRiver = function(req, res) {
  res.render('tours/mon-river')
}

exports.ohioRiver = function(req, res) {
  res.render('tours/ohio-river')
}

exports.requestGrouprate = function(req, res) {
  res.render('tours/request-grouprate')
}