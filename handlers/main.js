/**
 * Created by instancetype on 8/15/14.
 */
 /* jshint bitwise : true, eqeqeq : true, forin : true, noarg : true, noempty : true, nonew : true,
   asi : true, esnext : true, laxcomma : true, sub : true, browser : true, node : true, phantom : true */
const
  color = require('../lib/color')
, credentials = require('../credentials')
, emailService = require('../lib/email')(credentials)

exports.home = function(req, res) {
  res.render('home')
}

exports.about = function(req, res) {
  res.render(
    'about'
  , { color : color.getColor()
    , pageTestScript : 'qa/tests-about.js'
    }
  )
}

exports.nurseryRhyme = function(req, res) {
  res.render('nursery-rhyme')
}

exports.nurseryRhymeData = function(req, res) {
  res.json(
    { animal    : 'pelican'
    , bodyPart  : 'beak'
    , adjective : 'pointy'
    , noun      : 'sword'
    }
  )
}

exports.newsletter = function(req, res) {
  res.render('newsletter', { csrf: 'CSRF token forthcoming' })
}

exports.process = function(req, res) {

  if (req.xhr || req.accepts('json, html') === 'json') {
    res.send({ success : true })
  }
  else {
    console.log('Form (from querystring):', req.query.form)
    console.log('CSRF token (from hidden field):', req.body._csrf)
    console.log('Name (from name field):', req.body.name)
    console.log('Email (from email field):', req.body.email)
    res.redirect(303, '/thank-you')
  }
}

exports.mailtest = function(req, res) {
  emailService.send(
    'ayarunas@gmail.com'
  , 'Hello from Instancetype'
  , '<h1>Guess what?</h1>\n<p>Modularized HTML email works too!</p>'
  , function(err) {
      if (err) {
        req.session.flash =
          { type : 'danger'
          , intro : 'There was a problem.'
          , message : 'The message could not be sent.'
          }
        res.redirect(303, '/error')
      }
      else {
        req.session.flash =
          { type : 'success'
          , intro : 'Check your mailbox!'
          , message : 'You should receive a message momentarily.'
          }
        res.redirect(303, '/')
      }
    }
  )
}

exports.thankYou = function(req, res) {
  res.render('thank-you')
}

exports.error = function(req, res) {
  res.render('error')
}