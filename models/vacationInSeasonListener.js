/**
 * Created by instancetype on 8/14/14.
 */
 /* jshint bitwise : true, eqeqeq : true, forin : true, noarg : true, noempty : true, nonew : true,
   asi : true, esnext : true, laxcomma : true, sub : true, browser : true, node : true, phantom : true */
const
  mongoose = require('mongoose')

, vacationInSeasonListenerSchema = mongoose.Schema(
    { email : String
    , skus  : [ String ]
    }
  )

, VacationInSeasonListener = mongoose.model('VacationInSeasonListener', vacationInSeasonListenerSchema)

module.exports = VacationInSeasonListener
