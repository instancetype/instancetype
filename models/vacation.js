/**
 * Created by instancetype on 8/13/14.
 */
 /* jshint bitwise : true, eqeqeq : true, forin : true, noarg : true, noempty : true, nonew : true,
   asi : true, esnext : true, laxcomma : true, sub : true, browser : true, node : true, phantom : true */
const
  mongoose = require('mongoose')
, vacationSchema = mongoose.Schema(
    { name : String
    , slug : String
    , category : String
    , sku : String
    , description : String
    , priceInCents : Number
    , tags : [ String ]
    , isInSeason : Boolean
    , isAvailable : Boolean
    , requiresWaiver : Boolean
    , maximumGuests : Number
    , notes : String
    , packagesSold : Number
    }
  )
vacationSchema.methods.getDisplayPrice = function() {
  return '$' + (this.priceInCents / 100).toFixed(2)
}

var Vacation = mongoose.model('Vacation', vacationSchema)
module.exports = Vacation