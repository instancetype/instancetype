/**
 * Created by instancetype on 8/16/14.
 */
 /* jshint bitwise : true, eqeqeq : true, forin : true, noarg : true, noempty : true, nonew : true,
   asi : true, esnext : true, laxcomma : true, sub : true, browser : true, node : true, phantom : true */
const
  mongoose = require('mongoose')

, attractionSchema = mongoose.Schema(
    { name : String
    , description : String
    , location : { lat : Number
                 , lng : Number
                 }
    , history : { event : String
                , notes : String
                , email : String
                , date  : Date
                }
    , updateId : String
    , approved : Boolean
    }
  )

var Attraction = mongoose.model('Attraction', attractionSchema)
module.exports = Attraction