/* Created by instancetype on 8/8/14. */
 /* jshint bitwise : true, eqeqeq : true, forin : true, noarg : true, noempty : true, nonew : true,
   asi : true, esnext : true, laxcomma : true, sub : true, browser : true, node : true, phantom : true */
suite('Global Tests', function() {

  test('page has a valid title', function() {
    assert(
        document.title &&
        document.title.match(/\S/) &&
        document.title.toUpperCase() !== 'TODO'
    )
  })

})

