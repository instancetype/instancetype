/* Created by instancetype on 8/8/14. */
 /* jshint bitwise : true, eqeqeq : true, forin : true, noarg : true, noempty : true, nonew : true,
   asi : true, esnext : true, laxcomma : true, sub : true, browser : true, node : true, phantom : true */
suite('"About" Page Tests', function() {

  test('page contains a link to contact page', function() {
    assert($('a[href="/contact"]').length)
  })
})