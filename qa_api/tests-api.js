/**
 * Created by instancetype on 8/16/14.
 */
 /* jshint bitwise : true, eqeqeq : true, forin : true, noarg : true, noempty : true, nonew : true,
   asi : true, esnext : true, laxcomma : true, sub : true, browser : true, node : true, phantom : true */

// TEST INDEPENDENTLY:  mocha -u tdd -R spec qa_api/tests-api.js
const
  assert = require('chai').assert
, http = require('http')
, rest = require('restler')

suite('API tests', function() {

  var attraction =
    { lat : 40.445856
    , lng : -80.018205
    , name : 'Carnegie Science Center'
    , description : 'Museum & research center offers science-related exhibits, a planetarium, live shows & kids programs'
    , email : 'test@example.com'
    }

  var base = 'http://api.instancetype:3000'

  test('should be able to add an attraction', function(done) {
    rest.post(base + '/attraction', { data : attraction })
      .on('success', function(data) {
        assert.match(data.id, /\w/, 'id must be set')
        done()
      })
  })

  test('should be able to retrieve an attraction', function(done) {
    rest.post(base + '/attraction', { data : attraction })
      .on('success', function(data) {
        rest.get(base + '/attraction/' + data.id)
          .on('success', function(data) {
            assert(data.name === attraction.name)
            assert(data.description === data.description)
            done()
          })
      })
  })
})