/* Created by instancetype on 8/8/14. */
 /* jshint bitwise : true, eqeqeq : true, forin : true, noarg : true, noempty : true, nonew : true,
   asi : true, esnext : true, laxcomma : true, sub : true, browser : true, node : true, phantom : true */
module.exports = function(grunt) {

  [ 'grunt-cafe-mocha'
  , 'grunt-contrib-jshint'
  , 'grunt-exec'
  ].forEach(function(task) {
      grunt.loadNpmTasks(task)
    }
  )

  grunt.initConfig(

    { cafemocha : { all : { src : 'qa_main/tests-*.js', options : { ui : 'tdd' }}}

    , jshint    : { app : [ 'instancetype.js', 'public/js/**/*.js', 'lib/**/*.js' ]
                  , qa  : [ 'Gruntfile.js', 'public/qa/**/*.js', 'qa/**/*.js' ]
                  , options : { jshintrc : '.jshintrc' }
                  }

    }
  )

  grunt.registerTask('default', [ 'cafemocha', 'jshint' ])

}